package com.mycompany.hello;

import java.util.ArrayList;


public class Event {

    private String Location;
    private String Date;
    private String Type;

    public Event(String l,String d, String t)
    {
        Location=l;
        Date=d;
        Type=t;
    }

    public String getLocation()
    {
        return Location;
    }

    public String getDate()
    {
        return Date;
    }
    public String getType()
    {
        return Type;
    }

    public static ArrayList<Event> clean(ArrayList<Event> list)
    {
        ArrayList<Event> temp=new ArrayList<Event>();
        temp.add(list.get(0));
        boolean f=false;
        for(int i=0;i<list.size();i++)
        {
            for(int j=0;j<temp.size();j++)
            {
                if(!(list.get(i).getLocation().equals(temp.get(j).getLocation())&&list.get(i).getDate().equals(temp.get(j).getDate())))
                {
                    f=false;
                }
                else
                {
                    f=true;
                    break;
                }


            }
            if(f==false)
            {
                temp.add(list.get(i));
            }
        }
        int n=temp.size();
        int k;
        int low;
        Event t;
        for(int m=n;m>=0;m--)
        {
            for(int i=0;i<n-1;i++)
            {
                k=i+1;
               if(temp.get(i).Date.compareTo(temp.get(k).Date)<0)
               {
                   low=i;
                   t=temp.get(i);
                   temp.set(i,temp.get(k));
                   temp.set(k,t);
 
               }
            }

        }
 
        return temp;
    }

}