package com.mycompany.hello;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class SqlConnect {

    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement ps = null;
    private ResultSet result = null;

    public ArrayList<Event> getScgEvents(String d) {
        ArrayList<Event> Events = new ArrayList<Event>();
        try {

            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1/Decks?", "sqlUser", "Passwordsql");
            statement = connect.createStatement();
            ps = connect.prepareStatement("SELECT DISTINCT event, date, location FROM " + d + " ORDER BY date DESC ");
            result = ps.executeQuery();

            while (result.next()) {
                Event temp = new Event(result.getString(3), result.getString(2), result.getString(1));
                Events.add(temp);
            }

        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            try {
                result.close();
                statement.close();
                connect.close();
            } catch (Exception e) {

            }

        }
        return Events;
    }

    public ArrayList<Deck> getScgDecks(String Location, String Date, String d) {
        ArrayList<Deck> decks = new ArrayList<Deck>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1/Decks?", "sqlUser", "Passwordsql");
            statement = connect.createStatement();
            ps = connect.prepareStatement("SELECT * from " + d + " WHERE Location= '" + Location + "' " + "AND " + "Date= '" + Date + "'");
            result = ps.executeQuery();
            while (result.next()) {
                Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(7), result.getString(3), result.getString(4), d);
                decks.add(temp);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                statement.close();
                connect.close();
            } catch (Exception e) {

            }

        }
        return decks;
    }

    public String getScgDeckList(int id, String d) {
        String cards;
        String list = "";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1/Decks?", "sqlUser", "Passwordsql");
            statement = connect.createStatement();
            ps = connect.prepareStatement("SELECT * from " + d + " WHERE id=" + id);
            result = ps.executeQuery();

            while (result.next()) {
                list = result.getString(8);
            }

        } catch (Exception e) {

        } finally {
            try {
                result.close();
                statement.close();
                connect.close();
            } catch (Exception e) {

            }

        }

        return list;
    }

    public ArrayList<Event> getProStandardEvents() {
        ArrayList<Event> Events = new ArrayList<Event>();

        try {

            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1/Decks?", "sqlUser", "Passwordsql");
            statement = connect.createStatement();
            ps = connect.prepareStatement("SELECT DISTINCT event, date FROM gpStandard");
            result = ps.executeQuery();

            while (result.next()) {
                Event temp = new Event(result.getString(1), result.getString(2), "Pro Standard");
                Events.add(temp);
            }

        } catch (Exception e) {

        } finally {
            try {
                result.close();
                statement.close();
                connect.close();
            } catch (Exception e) {

            }

        }

        return Events;
    }

    public ArrayList<Deck> getProStandardDecks(String Event, String Date) {
        ArrayList<Deck> decks = new ArrayList<Deck>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1/Decks?", "sqlUser", "Passwordsql");
            statement = connect.createStatement();
            ps = connect.prepareStatement("SELECT * from gpStandard WHERE Event= '" + Event + "' " + "AND " + "Date= '" + Date + "'");
            result = ps.executeQuery();
            while (result.next()) {
                Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(4), result.getString(5), result.getString(3), "ProStandard");
                decks.add(temp);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                statement.close();
                connect.close();
            } catch (Exception e) {

            }

        }
        return decks;
    }

    public String getProStandardDeckList(int id) {
        String cards;
        String list = "";
        try {

            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1/Decks?", "sqlUser", "Passwordsql");
            statement = connect.createStatement();
            ps = connect.prepareStatement("SELECT * from gpStandard WHERE id=" + id);
            result = ps.executeQuery();

            while (result.next()) {
                list = result.getString(7);
            }

        } catch (Exception e) {

        } finally {
            try {
                result.close();
                statement.close();
                connect.close();
            } catch (Exception e) {

            }

        }

        return list;
    }

    public ArrayList<Deck> Search(String f, String n, String[] c) {
        ArrayList<Deck> decks = new ArrayList<Deck>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1/Decks?", "sqlUser", "Passwordsql");
            statement = connect.createStatement();

            if (f.equals("standard")) {

                String cards = "AND deck ";
                for (int i = 0; i < c.length; i++) {
                    cards = cards + " LIKE '%" + c[i] + "%' AND deck ";
                }

                cards = cards + " LIKE '%%'";
                ps = connect.prepareStatement("SELECT * from scg WHERE deckName LIKE '%" + n + "%' " + cards);

                result = ps.executeQuery();
                while (result.next()) {
                    Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(7), result.getString(3), result.getString(4), "Scg Standard Open");
                    decks.add(temp);
                }

                ps = connect.prepareStatement("SELECT * from scgStandardClassic WHERE deckName LIKE '%" + n + "%' " + cards);
                result = ps.executeQuery();
                while (result.next()) {
                    Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(7), result.getString(3), result.getString(4), "Scg Standard Classic");
                    decks.add(temp);
                }

                cards = "AND cards ";
                for (int i = 0; i < c.length; i++) {
                    cards = cards + " LIKE '%" + c[i] + "%' AND cards ";
                }

                cards = cards + " LIKE '%%'";
                ps = connect.prepareStatement("SELECT * from gpStandard WHERE name LIKE '%" + n + "%' " + cards);
                result = ps.executeQuery();
                while (result.next()) {
                    Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(4), result.getString(5), result.getString(3), "Pro Standard");
                    decks.add(temp);
                }

            } else if (f.equals("legacy")) {
                String cards = "AND deck ";
                for (int i = 0; i < c.length; i++) {
                    cards = cards + " LIKE '%" + c[i] + "%' AND deck ";
                }

                cards = cards + " LIKE '%%'";
                ps = connect.prepareStatement("SELECT * from scgLegacy WHERE deckName LIKE '%" + n + "%' " + cards);

                result = ps.executeQuery();
                while (result.next()) {
                    Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(7), result.getString(3), result.getString(4), "Scg Legacy Open");
                    decks.add(temp);
                }

                ps = connect.prepareStatement("SELECT * from scgLegacyClassic WHERE deckName LIKE '%" + n + "%' " + cards);
                result = ps.executeQuery();
                while (result.next()) {
                    Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(7), result.getString(3), result.getString(4), "Scg Legacy Classic");
                    decks.add(temp);
                }
            } else if (f.equals("modern")) {
                String cards = "AND deck ";
                for (int i = 0; i < c.length; i++) {
                    cards = cards + " LIKE '%" + c[i] + "%' AND deck ";
                }

                cards = cards + " LIKE '%%'";
                ps = connect.prepareStatement("SELECT * from scgModern WHERE deckName LIKE '%" + n + "%' " + cards);

                result = ps.executeQuery();
                while (result.next()) {
                    Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(7), result.getString(3), result.getString(4), "Scg Modern Open");
                    decks.add(temp);
                }
               
                ps = connect.prepareStatement("SELECT * from scgModernClassic WHERE deckName LIKE '%" + n + "%' " + cards);
                result = ps.executeQuery();
                while (result.next()) {
                    Deck temp = new Deck(result.getInt(1), result.getString(2), result.getString(6), result.getString(7), result.getString(3), result.getString(4), "Scg Modern Classic");
                    decks.add(temp);
                }
            }

        } catch (Exception e) {

        } finally {
            try {
                result.close();
                statement.close();
                connect.close();
            } catch (Exception e) {

            }

        }
        return decks;
    }

}
