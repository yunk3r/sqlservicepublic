package com.mycompany.hello;

public class Deck {

    private int Id;
    private String Name;
    private String Date;
    private String Location;
    private String Place;
    private String Player;
    private String event;

    public Deck(int i, String n, String d, String l, String p, String pl, String e) {
        Id = i;
        Name = n;
        Date = d;
        Location = l;
        Place = p;
        Player = pl;
        event = e;
    }

    public int getID() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getDate() {
        return Date;
    }

    public String getLocation() {
        return Location;
    }

    public String getPlace() {
        return Place;
    }

    public String getPlayer() {
        return Player;
    }

    public String getEvent() {
        return event;
    }

}
