package com.mycompany.hello;


import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SqlController {

 SqlConnect con=new SqlConnect();

    @RequestMapping("/EventScg")
    public Event[] EventScg(@RequestParam(value="event", defaultValue="scg") String event) {
        ArrayList<Event> temp=con.getScgEvents(event);
        Event [] array=new Event[temp.size()];
        array=temp.toArray(array);
        return array;
    }
    
    @RequestMapping("/EventPro")
    public Event[] EventPro() {
        ArrayList<Event> temp=con.getProStandardEvents();
        Event [] array=new Event[temp.size()];
        array=temp.toArray(array);
        return array;
    }
    
     @RequestMapping("/ScgDecks")
    public Deck[] getScgDecks(@RequestParam(value="Location", defaultValue="L") String Location,@RequestParam(value="Date", defaultValue="D") String Date,@RequestParam(value="d", defaultValue="scg") String d) {
        ArrayList<Deck> temp=con.getScgDecks(Location,Date,d);
        Deck [] array=new Deck[temp.size()];
        array=temp.toArray(array);
        return array;
    }
    
    @RequestMapping("/ProDecks")
    public Deck[] getProDecks(@RequestParam(value="Location", defaultValue="Null") String Location,@RequestParam(value="Date", defaultValue="Null") String Date) {
        ArrayList<Deck> temp=con.getProStandardDecks(Location,Date);
        Deck [] array=new Deck[temp.size()];
        array=temp.toArray(array);
        return array;
    }
    
     @RequestMapping("/ScgList")
    public String getScgDecks(@RequestParam(value="id", defaultValue="0") String id,@RequestParam(value="d", defaultValue="scg") String d) {
        return con.getScgDeckList(Integer.valueOf(id).intValue(),d);
        
    }
    
    @RequestMapping("/ProList")
    public String getProDecks(@RequestParam(value="id", defaultValue="0") String id) {
        return con.getProStandardDeckList(Integer.valueOf(id));
        
    }
    
     @RequestMapping("/Search")
    public Deck[] Search(@RequestParam(value="format", defaultValue="standard") String format,@RequestParam(value="name", defaultValue="") String name,
            @RequestParam(value="cards",defaultValue="") String cards) {
        String [] cardList=cards.split(",");
        ArrayList<Deck> temp=con.Search(format,name,cardList);
        Deck [] array=new Deck[temp.size()];
        array=temp.toArray(array);
        return array;
        }
}